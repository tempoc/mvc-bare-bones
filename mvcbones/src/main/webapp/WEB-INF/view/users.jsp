<%--
  Created by IntelliJ IDEA.
  User: tempoc
  Date: 9/4/12
  Time: 7:05 PM
--%>
<%@ include file="/WEB-INF/jsp/include.jsp" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title><fmt:message key="users.title"/></title>
</head>
<body>
<h1><fmt:message key="users.h1"/></h1>
<a href="<c:url value="users/create"/>"><fmt:message key="users.create.h1"/></a>
<a href="<c:url value="/"/>"><fmt:message key="home.h1"/></a>
<table>
    <thead>
        <tr>
            <th><fmt:message key="users.label.username"/></th>
        </tr>
    </thead>
    <c:forEach var="user" items="${users}">
        <tr>
            <td><c:out value="${user.username}"/></td>
            <td><a href="<c:url value="users/delete/${user.username}"/>"><fmt:message key="users.command.delete"/></a></td>
        </tr>
    </c:forEach>
</table>
</body>
</html>