<%--
  Created by IntelliJ IDEA.
  User: tempoc
  Date: 6/19/12
  Time: 11:43 AM
--%>
<%@ include file="/WEB-INF/jsp/include.jsp" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title><fmt:message key="home.title"/></title>
    </head>
    <body>
        <h1><fmt:message key="home.h1"/></h1>
        <p>The time is now: <c:out value="${now}"/> </p>
        <a href="<c:url value="users"/>"><fmt:message key="users.h1"/></a>
    </body>
</html>