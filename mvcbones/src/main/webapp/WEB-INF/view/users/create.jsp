<%--
  Created by IntelliJ IDEA.
  User: tempoc
  Date: 9/4/12
  Time: 2:40 PM
--%>
<%@ include file="/WEB-INF/jsp/include.jsp" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title><fmt:message key="users.create.title"/></title>

    <style type="text/css">
        .error { color: red; }
    </style>
</head>
<body>
<h1><fmt:message key="users.create.h1"/></h1>
<div>
    <form:form action="create" method="post" commandName="createUser">
        <table>
            <tr>
                <td>
                    <fmt:message key="users.create.label.username"/>:
                </td>
                <td>
                    <form:input path="username" />
                </td>
                <td>
                    <form:errors path="username" cssClass="error"/>
                </td>
            </tr>
            <tr>
                <td>
                    <fmt:message key="users.create.label.password"/>:
                </td>
                <td>
                    <form:password path="password" />
                </td>
                <td>
                    <form:errors path="password" cssClass="error"/>
                </td>
            </tr>
        </table>
        <br>
        <input type="submit" value="<fmt:message key="users.create.create-user"/>">
    </form:form>
</div>
<a href="<c:url value="/"/>"><fmt:message key="home.h1"/></a>
</body>
</html>