package com.stgutah.acme.parking.repo;

import com.stgutah.acme.parking.entity.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: tempoc
 * Date: 9/4/12
 * Time: 1:44 PM
 */
public interface UserRepository extends CrudRepository<User, Long> {
    List<User> findUserByUsername(String username);
}
