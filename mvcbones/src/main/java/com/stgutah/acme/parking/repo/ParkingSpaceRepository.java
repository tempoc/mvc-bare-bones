package com.stgutah.acme.parking.repo;

import com.stgutah.acme.parking.entity.ParkingSpace;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: tempoc
 * Date: 9/4/12
 * Time: 1:47 PM
 */
public interface ParkingSpaceRepository extends CrudRepository<ParkingSpace, Long> {
    List<ParkingSpace> findByLabel(String label);
}
