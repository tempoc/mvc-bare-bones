package com.stgutah.acme.parking.controller;

import com.stgutah.acme.parking.entity.User;
import com.stgutah.acme.parking.model.Users;
import com.stgutah.acme.parking.repo.UserRepository;
import com.stgutah.acme.parking.service.CreateUser;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.validation.Valid;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

/**
 * Created with IntelliJ IDEA.
 * User: tempoc
 * Date: 9/4/12
 * Time: 1:56 PM
 */
@Controller
@RequestMapping("/users")
public class UsersCrudController {
    protected Log log = LogFactory.getLog(getClass());

    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public void setup(Model model) throws ServletException {
        log.debug("Loading create user form.");
        model.addAttribute(new CreateUser());
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public String process(@Valid CreateUser createUser, BindingResult result, Model model) {
        log.debug("Processing form.");
        log.debug("Validating...");
        User user = null;
        String username = createUser.getUsername();
        if(!userRepository.findUserByUsername(username).isEmpty()) {
            result.addError(new FieldError("createUser", "username", username + " is already taken."));
        }
        try {
            user = new User(username, createUser.getPassword());
        } catch (NoSuchAlgorithmException e) {
            log.error(e);
            result.addError(new FieldError("createUser", "password", "Problem processing password."));
        } catch (UnsupportedEncodingException e) {
            log.error(e);
            result.addError(new FieldError("createUser", "password", "Problem processing password."));
        }
        if(result.hasErrors()) {
            log.debug("Validation failed. Errors:");
            if(log.isDebugEnabled()) {
                for(ObjectError error : result.getAllErrors()) {
                    log.debug(error);
                }
            }
            model.addAttribute(createUser);
            return "users/create";
        }

        log.debug("Validation succeeded.");
        log.debug(username);
        userRepository.save(user);

        return "redirect:/users";
    }

    @RequestMapping
    public ModelAndView users(){
        log.debug("Serving users page");
        Users users = new Users();
        for(User u : userRepository.findAll()) {
            users.add(u);
        }
        return new ModelAndView("users", "users", users);
    }

    @RequestMapping("/delete/{username}")
    public String delete(@PathVariable String username) {
        log.debug("Deleting " + username);
        userRepository.delete(userRepository.findUserByUsername(username));
        return "redirect:/users";
    }
}
