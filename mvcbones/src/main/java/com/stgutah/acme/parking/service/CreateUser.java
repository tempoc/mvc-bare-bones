package com.stgutah.acme.parking.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created with IntelliJ IDEA.
 * User: tempoc
 * Date: 9/4/12
 * Time: 2:32 PM
 */
public class CreateUser {
    protected final Log log = LogFactory.getLog(getClass());

    @NotNull
    @Size(min = 1)
    private String username;

//    TODO: fix password size and enable regexp validation because it's a pain during testing.
    @NotNull
    @Size(min = 1)
//    @Pattern(regexp = "^.*(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\\W]).*$")
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
        log.debug("Username set to " + this.username);
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
