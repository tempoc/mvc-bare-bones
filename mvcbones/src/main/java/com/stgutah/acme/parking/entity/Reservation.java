package com.stgutah.acme.parking.entity;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.persistence.*;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: tempoc
 * Date: 9/4/12
 * Time: 11:17 AM
 */
@Entity
public class Reservation {
    protected static Log log = LogFactory.getLog(Reservation.class);

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne(optional = false)
    private ParkingSpace parkingSpace;

    @Column
    @Temporal(TemporalType.DATE)
    private Date start;

    @Column
    @Temporal(TemporalType.DATE)
    private Date end;

    @ManyToOne(optional = false)
    private User user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ParkingSpace getParkingSpace() {
        return parkingSpace;
    }

    public void setParkingSpace(ParkingSpace parkingSpace) {
        this.parkingSpace = parkingSpace;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        log.debug("Attempting to set new start timestamp: " + start);
        if(end!=null && start.after(end)) {
            log.debug("Failed because end timestamp was " + end);
            throw new IllegalArgumentException("Start date must be set before end date.");
        }
        this.start = start;
        log.debug("Succeeded setting new start timestamp.");
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        log.debug("Attempting to set new end timestamp: " + end);
        if(start!=null && end.before(start)) {
            log.debug("Failed because start timestamp was " + end);
            throw new IllegalArgumentException("End date must be set after start date.");
        }
        this.end = end;
        log.debug("Succeeded setting new end timestamp.");
    }

    public boolean isInTheFuture() {
        log.debug("Reservation is in the future. " + this);
        return start!=null && start.after(new Date());
    }

    public boolean isInProgress() {
        log.debug("Reservation is in progress. " + this);
        return isDuringThisReservation(new Date());
    }

    public boolean isExpired() {
        log.debug("Reservation is expired. " + this);
        return end!=null && end.before(new Date());
    }

    public boolean isBooked(Date desiredStart, Date desiredEnd) {
        return (desiredStart !=null && start!=null && desiredEnd !=null && end!=null
                && (desiredStart.before(start) && desiredEnd.after(end)))
                || isDuringThisReservation(desiredStart)
                || isDuringThisReservation(desiredEnd);
    }

    private boolean isDuringThisReservation(Date date) {
        return date!=null && start!=null && end!=null && date.after(start) && date.before(end);
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Reservation");
        sb.append("{id=").append(id);
        sb.append(", start=").append(start);
        sb.append(", end=").append(end);
        sb.append('}');
        return sb.toString();
    }
}
