package com.stgutah.acme.parking.repo;

import com.stgutah.acme.parking.entity.ParkingSpace;
import com.stgutah.acme.parking.entity.Reservation;
import com.stgutah.acme.parking.entity.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: tempoc
 * Date: 9/4/12
 * Time: 1:49 PM
 */
public interface ReservationRepository extends CrudRepository<Reservation, Long> {
    List<Reservation> findByUserOrderByStartAsc(User user);
    List<Reservation> findByParkingSpaceOrderByStartAsc(ParkingSpace parkingSpace);
}
