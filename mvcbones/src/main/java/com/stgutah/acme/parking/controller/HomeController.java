package com.stgutah.acme.parking.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: tempoc
 * Date: 6/19/12
 * Time: 11:40 AM
 */
@Controller
public class HomeController {

    protected final Log log = LogFactory.getLog(getClass());

    @RequestMapping("/")
    public ModelAndView home() {

        String now = (new Date()).toString();
        log.debug("now= " + now);

        return new ModelAndView("home", "now", now);
    }
}
