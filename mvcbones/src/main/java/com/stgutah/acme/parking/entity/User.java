package com.stgutah.acme.parking.entity;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.persistence.*;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

/**
 * Created with IntelliJ IDEA.
 * User: tempoc
 * Date: 8/31/12
 * Time: 4:59 PM
 */
@Entity
public class User {
    protected static Log log = LogFactory.getLog(User.class);

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(unique = true)
    private String username;

    @Column
    private byte[] passhash;

    @OneToMany(mappedBy = "user")
    @OrderBy("start asc")
    private Collection<Reservation> reservations = new ArrayList<Reservation>();

    public User() {log.debug("new User()");}

    public User(String username, String password) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        this.username = username;
        this.passhash = hash(password);
        log.debug("new User(" +  username + ")");
    }

    public void setPasshash(String old, String password) throws UnsupportedEncodingException, NoSuchAlgorithmException, IllegalAccessException {
        log.debug("Attempting to set new password.");
        if(passhash!=null && passhash.length!=0 && !isPassword(old)) {
            log.debug("Old password was incorrect. Failed.");
            throw new IllegalAccessException("Bad password.");
        }
        passhash = hash(password);
        log.debug("New password set successfully.");
    }

    /**
     * Use this to encrypt the password. This is a test and is not designed for real world use. Unsafe.
     * @param s Candidate String to encrypt.
     * @return The MD5 hash of s.
     * @throws UnsupportedEncodingException
     * @throws NoSuchAlgorithmException
     */
    private static byte[] hash(String s) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        byte[] bytesOfMessage = s.getBytes("UTF-8");
        MessageDigest md = MessageDigest.getInstance("MD5");
        return md.digest(bytesOfMessage);
    }

    public boolean isPassword(String password) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        return Arrays.equals(passhash, hash(password));
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Collection<Reservation> getReservations() {
        return reservations;
    }
}
