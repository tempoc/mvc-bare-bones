package com.stgutah.acme.parking.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created with IntelliJ IDEA.
 * User: tempoc
 * Date: 9/4/12
 * Time: 11:12 AM
 */
@Entity
public class ParkingSpace {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private String label;

    @OneToMany(mappedBy = "parkingSpace")
    @OrderBy("start asc")
    private Collection<Reservation> reservations = new ArrayList<Reservation>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Collection<Reservation> getReservations() {
        return reservations;
    }
}
